document.addEventListener("DOMContentLoaded", function () {
  let totalPlanPrice;
  const inputFields = [
    {
      id: "name",
      regex: null,
      errorElement: "name",
    },
    {
      id: "email",
      regex: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
      errorElement: "email",
    },
    {
      id: "phone-number",
      regex: /^([0-9]+(?:[ -]?[0-9]+)*)$/,
      errorElement: "phone",
    },
  ];

  function validateInputFields() {
    let hasErrors = false;

    inputFields.forEach((field) => {
      const input = document.getElementById(field.id);
      const inputValue = input.value.trim();
      const errorElement = input.previousElementSibling.querySelector(".error");
      console.log(errorElement);

      if (inputValue === "") {
        hasErrors = true;
        errorElement.style.display = "block";
        input.style.borderColor = "red";
      } else {
        errorElement.style.display = "none";
        input.style.borderColor = "";
      }

      if (field.regex && !field.regex.test(inputValue)) {
        hasErrors = true;
        errorElement.style.display = "block";
        input.style.borderColor = "red";
      }
    });

    return hasErrors;
  }

  const checkbox = document.getElementById("checkbox");
  const prices = [
    { monthly: 9, yearly: 90 },
    { monthly: 12, yearly: 120 },
    { monthly: 15, yearly: 150 },
  ];

  checkbox.addEventListener("change", function () {
    const price1 = document.getElementById("price1");
    const price2 = document.getElementById("price2");
    const price3 = document.getElementById("price3");

    if (this.checked) {
      price1.innerHTML = `<li>$${prices[0].yearly}/year</li><li>2 months free</li>`;
      price2.innerHTML = `<li>$${prices[1].yearly}/year</li><li>2 months free</li>`;
      price3.innerHTML = `<li>$${prices[2].yearly}/year</li><li>2 months free</li>`;
    } else {
      price1.innerHTML = `<li>$${prices[0].monthly}/mo</li>`;
      price2.innerHTML = `<li>$${prices[1].monthly}/mo</li>`;
      price3.innerHTML = `<li>$${prices[2].monthly}/mo</li>`;
    }
  });

  function goToNextPage(currentPageClass, nextPageClass) {
    const currentPage = document.querySelector(`.${currentPageClass}`);
    const nextPage = document.querySelector(`.${nextPageClass}`);

    currentPage.style.display = "none";
    nextPage.style.display = "block";

    console.log("current", currentPage);
    console.log("next", nextPage);
  }
  const monthlyPrices = ["+$1/mo", "+$2/mo", "+$2/mo"];
  const yearlyPrices = ["+$10/yr", "+$20/yr", "+$20/yr"];

  function updatePrices() {
    const checkbox = document.getElementById("checkbox");
    const toggleCheck = checkbox.checked;

    const priceElements = document.querySelectorAll(".service .price");
    priceElements.forEach((priceElement, index) => {
      priceElement.textContent = toggleCheck
        ? yearlyPrices[index]
        : monthlyPrices[index];
    });
  }

  function updateTotalPrice() {
    const selectedPlanName = document.querySelector(
      ".choosed-pack .choosed-container .selected-plan .plan-name"
    );
    const selectedPlanPrice = document.querySelector(
      ".choosed-pack .choosed-container .selected-plan .plan-price"
    );
    const allPlans = document.querySelectorAll(
      "input[type='radio'][name='secondPage']"
    );
    const allServices = document.querySelectorAll("input[type='checkbox']");

    let planName = "";
    let planPrice = 0;

    allPlans.forEach((plan) => {
      if (plan.checked) {
        const aboutOption = plan.parentElement.querySelector(".about-option");
        planName = aboutOption.querySelector("h4").textContent;
        const planIndex =
          plan.id === "arcade" ? 0 : plan.id === "advanced" ? 1 : 2;
        planPrice = checkbox.checked
          ? prices[planIndex].yearly
          : prices[planIndex].monthly;
      }
    });
    console.log(planPrice);
    totalPlanPrice = planPrice;
    selectedPlanPrice.textContent = `$${planPrice}${
      checkbox.checked ? "/year" : "/mo"
    }`;
    selectedPlanName.textContent = `${planName} (${
      checkbox.checked ? "Yearly" : "Monthly"
    })`;
  }

  function addOns() {
    const allServices = document.querySelectorAll(
      "input[type='checkbox'][name='third-page']"
    );
    const serviceDivs = document.querySelectorAll(".charges > div");
    const checkbox = document.getElementById("checkbox");

    allServices.forEach((service, index) => {
      if (service.checked) {
        serviceDivs[index].style.display = "flex";
      } else {
        serviceDivs[index].style.display = "none";
      }
    });

    if (checkbox.checked) {
      const servicePrices = document.querySelectorAll(
        ".charges .service-price"
      );

      servicePrices.forEach((priceElement, index) => {
        priceElement.textContent = yearlyPrices[index];
      });
    } else {
      const servicePrices = document.querySelectorAll(
        ".charges .service-price"
      );
      servicePrices.forEach((priceElement, index) => {
        priceElement.textContent = monthlyPrices[index];
      });
    }
  }

  function totalMoneyToPay() {
    const allServices = document.querySelectorAll(
      "input[type='checkbox'][name='third-page']"
    );
    const checkbox = document.getElementById("checkbox");

    const selectedPrices = checkbox.checked ? yearlyPrices : monthlyPrices;

    let money = 0;

    selectedPrices.forEach((price, index) => {
      if (allServices[index].checked) {
        const numericPrice = parseFloat(price.replace(/[^\d.]/g, ""));
        money += numericPrice;
      }
    });

    money += parseFloat(totalPlanPrice);

    const totalPriceElement = document.querySelector(".total-price");
    totalPriceElement.textContent = `$${money.toFixed(2)}/${
      checkbox.checked ? "year" : "mo"
    }`;
  }

  function shiftActive(current, other) {
    const currentElement = document.querySelector(`${current}`);
    const otherElement = document.querySelector(`${other}`);

    currentElement.classList.remove("active");
    otherElement.classList.add("active");
  }

  function attachEventListeners() {
    const nextBtn1 = document.querySelector(".next-btn.one");
    const backBtn1 = document.querySelector(".back-btn.one");
    const nextBtn2 = document.querySelector(".next-btn.two");
    const backBtn2 = document.querySelector(".back-btn.two");
    const nextBtn3 = document.querySelector(".next-btn.three");
    const backBtn3 = document.querySelector(".back-btn.three");
    const checkbox = document.getElementById("checkbox");
    const confirm = document.querySelector(".next-btn.four");
    const change = document.querySelector(".change-btn");

    nextBtn1.addEventListener("click", function () {
      if (!validateInputFields()) {
        goToNextPage("page.one", "page.two");
        shiftActive(".step.one", ".step.two");
      }
    });

    backBtn1.addEventListener("click", function () {
      goToNextPage("page.two", "page.one");
      shiftActive(".step.two", ".step.one");
    });

    nextBtn2.addEventListener("click", function () {
      goToNextPage("page.two", "page.three");
      updatePrices();
      updateTotalPrice();
      shiftActive(".step.two", ".step.three");
    });

    backBtn2.addEventListener("click", function () {
      goToNextPage("page.three", "page.two");
      shiftActive(".step.three", ".step.two");
    });

    nextBtn3.addEventListener("click", function () {
      goToNextPage("page.three", "page.four");
      addOns();
      totalMoneyToPay();
      shiftActive(".step.three", ".step.four");
    });

    backBtn3.addEventListener("click", function () {
      goToNextPage("page.four", "page.three");
      shiftActive(".step.four", ".step.three");
    });
    confirm.addEventListener("click", function () {
      goToNextPage("page.four", "page.five");
    });
    change.addEventListener("click", function () {
      goToNextPage("page.four", "page.two");
      shiftActive(".step.four", ".step.two");
    });
    checkbox.addEventListener("change", updatePrices);
  }

  attachEventListeners();
});
